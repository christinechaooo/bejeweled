$(document).ready(function(){
	var gems = new Array();
	var gemSelected = -1,
		firstGemRow = -1,
		firstGemCol = -1,
		secondGemRow = -1,
		secondGemCol = -1,
		removeArray = new Array(),
		fillingArray = new Array();

	createBoard();

	function createBoard() {
		//init board
		for (var i = 0; i < 8; i++) {
			gems[i] = new Array();
			for (var j = 0; j < 8; j++) {
				gems[i][j] = -1;
				gems[i][j] = Math.floor(Math.random() * 7);
				if(i > 1) {
					while(gems[i-1][j] === gems[i][j] && gems[i-2][j] === gems[i][j]){
						gems[i][j] = Math.floor(Math.random() * 7);
					}
				}

				if(j > 1) {
					while(gems[i][j-1] === gems[i][j] && gems[i][j-2] === gems[i][j]){
						gems[i][j] = Math.floor(Math.random() * 7);
						if(i > 1) {
							while(gems[i-1][j] === gems[i][j] && gems[i-2][j] === gems[i][j]){
								gems[i][j] = Math.floor(Math.random() * 7);
							}
						}
					}
				}
				
				$("#board").append('<div class="gem" id="gem'+i+'_'+j+'" data-value="' + gems[i][j] + '"></div>');
				$("#gem"+i+"_"+j).css({'background-image':'url(../images/sprite_gems.png)', 'background-position': gems[i][j] * -60 + 'px 0', 'top':i * 60, 'left':j * 60});
			};
		};
	}

	$(".gem").on({
		mouseover:function(){
			// $(this).css('background-position', $(this).data('value') * -60 + 'px -60px');
		},
		mouseout:function() {
			// $(this).css('background-position', $(this).data('value') * -60 + 'px 0');
		},
		click:function() {
			$("#gem-states").css({'top':$(this).css('top'), 'left':$(this).css('left'), 'display':'block'});
			if(gemSelected < 0) {
				firstGemRow = parseInt($(this).css('top')) / 60;
				firstGemCol = parseInt($(this).css('left')) / 60;
				console.log('1.1',firstGemRow, firstGemCol);
				gemSelected = 1;
			} else {
				secondGemRow = parseInt($(this).css('top')) / 60;
				secondGemCol = parseInt($(this).css('left')) / 60;
				if((Math.abs(secondGemRow - firstGemRow) === 1 && Math.abs(secondGemCol - firstGemCol) === 0)||(Math.abs(secondGemRow - firstGemRow) === 0 && Math.abs(secondGemCol - firstGemCol) === 1)) {
					$("#gem-states").css('display','none');
					
					console.log('2', secondGemRow, secondGemCol);
					gemSwap(1);
					gemSelected = -1;
				} else {
					firstGemRow = parseInt($(this).css('top')) / 60;
					firstGemCol = parseInt($(this).css('left')) / 60;
					gemSelected = 1;
					console.log('1.2',firstGemRow, firstGemCol, gemSelected);
				}
			}
		}
	})

	function gemSwap(revert) {
		console.log('gemSwap', firstGemRow, firstGemCol, secondGemRow, secondGemCol)
		var dir;
		if(Math.abs(firstGemRow - secondGemRow) === 1) {
			dir = (firstGemRow - secondGemRow);
			TweenMax.to($("#gem"+firstGemRow+"_"+firstGemCol), 0.3, {'top':'+=' + 60 * -dir});
			TweenMax.to($("#gem"+secondGemRow+"_"+secondGemCol), 0.3, {'top':'+=' + 60 * dir});
		} else if (Math.abs(firstGemCol - secondGemCol) === 1) {
			dir = (firstGemCol - secondGemCol);
			TweenMax.to($("#gem"+firstGemRow+"_"+firstGemCol), 0.3, {'left':'+=' + 60 * -dir});
			TweenMax.to($("#gem"+secondGemRow+"_"+secondGemCol), 0.3, {'left':'+=' + 60 * dir});
		}
		$("#gem"+firstGemRow+"_"+firstGemCol).attr("id","tempID");
		$("#gem"+secondGemRow+"_"+secondGemCol).attr("id", "gem"+firstGemRow+"_"+firstGemCol);
		$("#tempID").attr("id", "gem"+secondGemRow+"_"+secondGemCol);
		var temp = gems[firstGemRow][firstGemCol];
        gems[firstGemRow][firstGemCol] = gems[secondGemRow][secondGemCol];
        gems[secondGemRow][secondGemCol] = temp;
		if(revert === 1) {
			removeArray = [];
			TweenMax.to(this, 0.3, {onComplete:checkForMatch});
		}
	}

	function checkForMatch() {
		console.log('checkForMatch', isMatch(firstGemRow, firstGemCol), isMatch(secondGemRow, secondGemCol))
		if(isMatch(firstGemRow, firstGemCol)||isMatch(secondGemRow, secondGemCol)) {
			if(isMatch(firstGemRow, firstGemCol)) {
				removeGems(firstGemRow, firstGemCol);
			}

			if(isMatch(secondGemRow, secondGemCol)) {
				removeGems(secondGemRow, secondGemCol);
			}
			
		} else {
			gemSwap(-1);
		}
		
	}

	function removeGems(i, j) {
		var value = gems[i][j],
			counter = i;
			vArray = new Array(),
			hArray = new Array();
		removeArray.push("#gem"+i+"_"+j);
		gems[i][j] = -1;
		while(counter > 0 && gems[counter-1][j] === value) {
			vArray.push("#gem"+(counter-1)+"_"+j);
			gems[counter-1][j] = -1;
			counter--;
		}
		counter = i;
		while(counter < 7 && gems[counter+1][j] === value) {
			vArray.push("#gem"+(counter+1)+"_"+j);
			gems[counter+1][j] = -1;
			counter++;
		}

		counter = j;
		while(counter > 0 && gems[i][counter-1] === value) {
			hArray.push("#gem"+i+"_"+(counter-1));
			gems[i][counter-1] = -1;
			counter--;
		}
		counter = j;
		while(counter < 7 && gems[i][counter+1] === value) {
			hArray.push("#gem"+i+"_"+(counter+1));
			gems[i][counter+1] = -1;
			counter++;
		}
		if(vArray.length > 1 && hArray.length > 1) {
			removeArray = vArray.concat(hArray);
		} else if(vArray.length > 1) {
			removeArray = vArray;
		} else {
			removeArray = hArray;
		}
		removeArray.push("#gem"+i+"_"+j);
		gems[i][j] = -1;
		
		console.log(removeArray);
		TweenMax.allTo(removeArray, 0.3, {autoAlpha:0, delay:0.1});
		TweenMax.to(this, 0.4, {onComplete:function(){
			for (var k = 0; k < removeArray.length; k++) {
				// var div = document.getElementById(removeArray[k].slice(1));
				// div.parentNode.removeChild(div);
				$(removeArray[k]).css({"background-image":"none", "opacity":"1", "visibility":"visible", 'top':-60});
				$(removeArray[k]).attr("id", "temp");
			};
			dropGems();
		}})
		
		
	}

	function dropGems() {
		fillingArray = [];
		for (var j = 0; j < 8; j++) {
			var gap = 0;
			var gapStart = 0;
			var dropArray = new Array();
			for (var i = 0; i < 8; i++) {
				if(gems[i][j] === -1) {
					gap++;
					if(gap === 1) {
						gapStart = i;
					}
				}
			};
			// console.log(dropArray);
			if(gap > 0) {
				console.log('gapStart', gapStart);
				// TweenMax.to(dropArray, 0.1 * (gap + 1), {'top':'+='+ gap * 60, delay:0.4});
				for (var k = gapStart - 1; k >= 0; k--) {
					// console.log(k, j);
					$('#gem'+k+"_"+j).attr('id', 'gem'+(k+gap)+'_'+j);
					dropArray.push('#gem'+(k+gap)+'_'+j);
					// console.log(dropArray[k], 'gem'+(k+gap)+'_'+j)
					gems[k+gap][j] = gems[k][j];
					gems[k][j] = -1;
				};
				for (var m = 0; m < gap; m++) {
					gems[m][j] = Math.floor(Math.random() * 7);
					// $("#board").append('<div class="gem" id="gem'+m+'_'+j+'" data-value="' + gems[m][j] + '"></div>');
					$("#temp").attr("id", "gem"+m+"_"+j);
					$("#gem"+m+"_"+j).css({'background-image':'url(../images/sprite_gems.png)', 'background-position': gems[m][j] * -60 + 'px 0', 'top':-60 * (gap - m), 'left':j * 60});
					dropArray.push('#gem'+m+'_'+j);
				};
				// console.log(dropArray);
				// for (var n = 0; n < dropArray.length; n++) {
				// 	console.log(dropArray[n], $(dropArray[n]).css('top'));
				// 	TweenMax.to(dropArray[n], 0.3, {'top':'+='+ gap * 60, delay:0.3, onCompleteParams:[dropArray[n]], onComplete:function(id){
				// 		console.log(id, $(id).css('top'));
				// 	}});
					
				// };
				TweenMax.allTo(dropArray, 0.1 * (gap + 1), {'top':'+='+ gap * 60, delay:0.3, ease:Power2.easeInOut});
			}
			fillingArray.push(gap);
			// console.log(j, gap, dropArray);
		};
		console.log(fillingArray);
		checkMatrix();
		// addNewGems();
	}

	function addNewGems() {
		for (var j = 0; j < fillingArray.length; j++) {
			for (var i = 0; i < fillingArray[j]; i++) {
				gems[i][j] = Math.floor(Math.random() * 7);
				$("#board").append('<div class="gem" id="gem'+i+'_'+j+'" data-value="' + gems[i][j] + '"></div>');
				$("#gem"+i+"_"+j).css({'background-image':'url(../images/sprite_gems.png)', 'background-position': gems[i][j] * -60 + 'px 0', 'top':-60, 'left':j * 60});
				console.log($("#gem"+i+"_"+j).css('opacity'))
				TweenMax.to("#gem"+i+"_"+j, 0.1 * (fillingArray[j] + 1), {'top':i * 60, delay:0.4});
			};
		};

		checkMatrix();
	}

	function checkMatrix() {
		for (var i = 0; i < 8; i++) {
			var row = "";
			for (var j = 0; j < 8; j++) {
				row += gems[i][j] + ',';
			};
			console.log(row);
		};
	}

	function isVerticalMatch(i, j) {
		var value = gems[i][j],
			matchNum = 0,
			row = i;
		while(row > 0 && gems[row-1][j] === value) {
			// if(removeArray.indexOf("#gem"+(row+1)+"_"+j) === -1) {
			// 	removeArray.push("#gem"+(row+1)+"_"+j);
			// }
			// console.log($("#gems"+(row-1)+"_"+j))
			matchNum++;
			row--;
		}
		row = i;
		while(row < 7 && gems[row+1][j] === value) {
			// if(removeArray.indexOf("#gem"+(row+1)+"_"+j) === -1) {
			// 	removeArray.push("#gem"+(row+1)+"_"+j);
			// }
			// console.log($("#gems"+(row+1)+"_"+j))
			matchNum++;
			row++;
		}
		// console.log('vertical match', matchNum);
		return matchNum > 1;
	}

	function isHorizontalMatch(i, j) {
		var value = gems[i][j],
			matchNum = 0,
			col = j;
		while(col > 0 && gems[i][col-1] === value) {
			// if(removeArray.indexOf("#gem"+i+"_"+(col-1)) === -1) {
			// 	removeArray.push("#gem"+i+"_"+(col-1));
			// }
			// console.log($("#gems"+i+"_"+(col-1)))
			matchNum++;
			col--;
		}
		col = j;
		while(col < 7 && gems[i][col+1] === value) {
			// if(removeArray.indexOf("#gem"+i+"_"+(col+1)) === -1) {
			// 	removeArray.push("#gem"+i+"_"+(col+1));
			// }
			// console.log($("#gems"+i+"_"+(col+1)))
			matchNum++;
			col++;
		}
		// console.log('horizontal match', matchNum);
		return matchNum > 1;
	}

	function isMatch(i, j){
		return isVerticalMatch(i,j)||isHorizontalMatch(i,j);
	}
});